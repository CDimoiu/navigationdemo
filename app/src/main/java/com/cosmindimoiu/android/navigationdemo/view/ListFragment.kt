package com.cosmindimoiu.android.navigationdemo.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.cosmindimoiu.android.navigationdemo.R
import com.cosmindimoiu.android.navigationdemo.viewmodel.ListViewModel
import kotlinx.android.synthetic.main.fragment_list.*

class ListFragment : Fragment() {

    private lateinit var viewModel: ListViewModel
    private val elementsListAdapter = ElementsListAdapter(arrayListOf())

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(ListViewModel::class.java)

        recycler_view.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = elementsListAdapter
        }

        refresh_layout.setOnRefreshListener {
            refresh_layout.isRefreshing = false
            viewModel.refresh()
        }

        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.elements.observe(viewLifecycleOwner, Observer { elements ->
            elementsListAdapter.updateElements(elements)
        })

        viewModel.isLoading.observe(viewLifecycleOwner, Observer { isLoading ->
            if (isLoading) {
                progress_bar.visibility = View.VISIBLE
                recycler_view.visibility = View.GONE
            } else {
                progress_bar.visibility = View.GONE
                recycler_view.visibility = View.VISIBLE
            }
        })
    }
}