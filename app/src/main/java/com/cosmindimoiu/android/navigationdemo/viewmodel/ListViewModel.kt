package com.cosmindimoiu.android.navigationdemo.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.random.Random

class ListViewModel : ViewModel() {

    private val _elements = MutableLiveData(generateElements())
    val elements: LiveData<List<String>>
        get() = _elements

    private val _isLoading = MutableLiveData(false)
    val isLoading: LiveData<Boolean>
        get() = _isLoading

    private fun generateElements(): List<String> {
        val elementsList = ArrayList<String>()

        val maxRange: Int = Random.nextInt(20)

        for (i in 0..maxRange) {
            elementsList.add("Element: ${Random.nextInt(100)}")
        }

        return elementsList
    }

    fun refresh() {
        GlobalScope.launch(Dispatchers.Main) {
            _isLoading.value = true
            delay(2000)
            _elements.value = generateElements()
            _isLoading.value = false
        }
    }
}