package com.cosmindimoiu.android.navigationdemo.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.cosmindimoiu.android.navigationdemo.R
import com.cosmindimoiu.android.navigationdemo.databinding.ItemListBinding

class ElementsListAdapter(private val elements: List<String>) :
    RecyclerView.Adapter<ElementsListAdapter.ElementViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ElementViewHolder {
        val inflater: LayoutInflater = LayoutInflater.from(parent.context)
        val view =
            DataBindingUtil.inflate<ItemListBinding>(inflater, R.layout.item_list, parent, false)
        return ElementViewHolder(view)
    }

    override fun getItemCount() = elements.size

    override fun onBindViewHolder(holder: ElementViewHolder, position: Int) {
        holder.view.element = elements[position]
    }

    fun updateElements(newElements: List<String>) {
        (elements as ArrayList).clear()
        elements.addAll(newElements)
        notifyDataSetChanged()
    }

    class ElementViewHolder(var view: ItemListBinding) : RecyclerView.ViewHolder(view.root)
}